from app import app
from flask import render_template

@app.route('/')
@app.route('/index')
def index():
    return "Hello, World!"


@app.route('/page')
def page():
    return "Hello, World! THIS IS A PAGE"

@app.route('/load')
def load():
    return render_template('/home/anjaly/designProject/microblog/app/templates/loadImage.html')
