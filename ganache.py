from web3 import Web3
ganache_url="http://127.0.0.1:7545"
web3=Web3(Web3.HTTPProvider(ganache_url))
print(web3.isConnected())
account_1="0xf43428f10DeFeAF088f0d088Ee5DA385A6755445"
account_2="0x321E019182631986cB7B8A0c61E1E80513022352"
private_key="e20ed486feb932d7b82143abaac3b6a6575b07fdef2bee75d960c491611c8d0f"
nonce=web3.eth.getTransactionCount(account_1)

tx={
	'nonce':nonce,
	'to':account_2,
	'value':web3.toWei(1,'ether'),
	'gas':2000000,
	'gasPrice':web3.toWei('50','gwei')
}
signed_tx=web3.eth.account.signTransaction(tx,private_key)
tx_hash=web3.eth.sendRawTransaction(signed_tx.rawTransaction)
print(tx_hash)
